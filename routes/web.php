<?php

use App\Http\Controllers\Admin\AuditTrailController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\MediaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function (){
	return redirect()->route('dashboard.index');
})->name('home');


Route::group(['namespace' => 'App\Http\Controllers\Admin', 'middleware' => 'auth', 'verify' => TRUE],
	function (){
		Route::name('dashboard.')->group(function (){
			Route::get('/dashboard', [DashboardController::class, 'index'])->name('index');
		});

		Route::name('audit.')->group(function (){
			Route::get('/audit', [AuditTrailController::class, 'index'])->name('index');
		});

		Route::group(['prefix' => 'users'], function (){
			Route::get('/', 'UsersController@index')->name('users.index');
			Route::get('/create', 'UsersController@create')->name('users.create');
			Route::post('/create', 'UsersController@store')->name('users.store');
			Route::get('/{user}/show', 'UsersController@show')->name('users.show');
			Route::get('/{user}/edit', 'UsersController@edit')->name('users.edit');
			Route::patch('/{user}/update', 'UsersController@update')->name('users.update');
			Route::delete('/{user}/delete', 'UsersController@destroy')->name('users.destroy');
		});
		Route::group(['prefix' => 'roles'], function (){
			Route::get('/', 'RolesController@index')->name('roles.index');
			Route::get('/create', 'RolesController@create')->name('roles.create');
			Route::post('/create', 'RolesController@store')->name('roles.store');
			Route::get('/{role}/show', 'RolesController@show')->name('roles.show');
			Route::get('/{role}/edit', 'RolesController@edit')->name('roles.edit');
			Route::patch('/{role}/update', 'RolesController@update')->name('roles.update');
			Route::delete('/{role}/delete', 'RolesController@destroy')->name('roles.destroy');
		});
		Route::group(['prefix' => 'permissions'], function (){
			Route::get('/', 'PermissionsController@index')->name('permissions.index');
			Route::get('/create', 'PermissionsController@create')->name('permissions.create');
			Route::post('/create', 'PermissionsController@store')->name('permissions.store');
			Route::get('/{permission}/show', 'PermissionsController@show')->name('permissions.show');
			Route::get('/{permission}/edit', 'PermissionsController@edit')->name('permissions.edit');
			Route::patch('/{permission}/update', 'PermissionsController@update')
			     ->name('permissions.update');
			Route::delete('/{permission}/delete', 'PermissionsController@destroy')
			     ->name('permissions.destroy');
		});
		Route::group(['prefix' => 'profile'], function (){
			Route::get('/', 'ProfileController@profile')->name('profile.index');
			Route::patch('/{user}', 'ProfileController@update')->name('profile.update');
		});
	});
Route::any('/ckfinder/connector',
	'\CKSource\CKFinderBridge\Controller\CKFinderController@requestAction')
     ->name('ckfinder_connector');

Route::any('/ckfinder/browser',
	'\CKSource\CKFinderBridge\Controller\CKFinderController@browserAction')
     ->name('ckfinder_browser');

Route::any('/ckfinder/examples/{example?}',
	'CKSource\CKFinderBridge\Controller\CKFinderController@examplesAction')
     ->name('ckfinder_examples');

Route::name('media.')->group(function (){
	Route::get('/media', [MediaController::class, 'index'])->name('index');
});