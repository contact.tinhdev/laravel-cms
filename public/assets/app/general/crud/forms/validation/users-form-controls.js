// Class definition

var KTFormControls = function () {
	// Private functions

	var demo1 = function () {
		$("#user-form").validate({
			// define validation rules
			rules: {
				name: {
					required: true,
				},
				email: {
					required: true,
					email: true,
					minlength: 10
				},
			},
			messages: {
				name: {
					required: "Họ và tên không được để trống",
				},
				email: {
					required: "Email không được để trống",
					email: "Email chưa đúng định dạng",
					minlength: "Email chưa đúng định dạng",
				}
			},

			//display error alert on form submit
			invalidHandler: function (event, validator) {
				var alert = $('#kt_form_1_msg');
				alert.removeClass('kt--hide').show();
				KTUtil.scrollTop();
			},

			submitHandler: function (form) {
				form[0].submit(); // submit the form
			}
		});
	}


	return {
		// public functions
		init: function () {
			demo1();
		}
	};
}();

jQuery(document).ready(function () {
	KTFormControls.init();
});