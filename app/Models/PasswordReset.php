<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PasswordReset
 */
class PasswordReset extends Model{

	use HasFactory;

	public $timestamps = FALSE;
	public $primaryKey = NULL;

	protected $fillable = [
		'email',
		'token',
	];
}
