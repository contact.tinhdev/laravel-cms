<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/**
 * Class RolesController
 */
class RolesController extends Controller{


	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
	 */
	public function index(Request $request){
		$roles = Role::orderBy('id', 'DESC')->paginate(5);

		return view('partials.auth.roles.index', compact('roles'))
			->with('i', ($request->input('page', 1) - 1) * 5);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
	 */
	public function create(){
		$permissions = Permission::get();

		return view('partials.auth.roles.create', compact('permissions'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function store(Request $request){
		$this->validate($request, [
			'name'       => 'required|unique:roles,name',
			'permission' => 'required',
		]);

		$role = Role::create(['name' => $request->get('name')]);

		$role->syncPermissions($request->get('permission'));

		return redirect()->route('roles.index')
		                 ->with('success', 'Role created successfully');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Spatie\Permission\Models\Role $role
	 *
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
	 */
	public function show(Role $role){
		$role            = $role;
		$rolePermissions = $role->permissions;

		return view('partials.auth.roles.show', compact('role', 'rolePermissions'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Spatie\Permission\Models\Role $role
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
	 */
	public function edit(Role $role){
		$role            = $role;
		$rolePermissions = $role->permissions->pluck('name')->toArray();
		$permissions     = Permission::get();

		return view('partials.auth.roles.edit', compact('role', 'rolePermissions', 'permissions'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Spatie\Permission\Models\Role $role
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function update(Role $role, Request $request){
		$this->validate($request, [
			'name'       => 'required',
			'permission' => 'required',
		]);

		if ($role->update($request->only('name')) && $role->syncPermissions($request->get('permission'))){
			return redirect()->route('roles.index')
			                 ->with('success', 'Role cập nhật thành công!');
		}else{
			return redirect()->back()
			                 ->with('error', 'Role cập nhật không thành công!');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Spatie\Permission\Models\Role $role
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy(Role $role)
	: \Illuminate\Http\RedirectResponse{
		$role->delete();

		return redirect()->route('roles.index')
		                 ->with('success', 'Role deleted successfully');
	}
}
