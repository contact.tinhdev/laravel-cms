<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

/**
 * Class ProfileController
 */
class ProfileController extends Controller{

	/**
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
	 */
	public function profile(){
		return view('partials.profile.update');
	}

	/**
	 * @param \App\Http\Requests\UpdateUserRequest $request
	 * @param $user
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
	 */
	public function update(UpdateUserRequest $request, $user){
		$info = User::find($user);
		if ($request->password !== NULL){
			$request->merge([
				'password' => Hash::make($request->password),
			]);
		}

		if ($info->update($request->all())){
			return redirect()->back()->with('success', __('Nhân viên được cập nhật thành công'));
		}else{
			return redirect()
				->back()
				->withInput($request->all())
				->with('success', __('Nhân viên được cập nhật thành công'));
		}
	}
}
