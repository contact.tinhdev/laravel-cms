<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Spatie\Permission\Models\Role;

/**
 * Class UsersController
 */
class UsersController extends Controller{

	/**
	 * Display all users
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
	 */
	public function index(){
		$users = User::latest()->paginate(10);

		return view('partials.auth.users.index', compact('users'));
	}

	/**
	 * Show form for creating user
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
	 */
	public function create(){
		return view('partials.auth.users.create');
	}

	/**
	 * Store a newly created user
	 *
	 * @param User $user
	 * @param StoreUserRequest $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(User $user, StoreUserRequest $request){
		$user->create(array_merge($request->validated(), [
			'password' => 'ThisIsPassword'
		]));
		return redirect()->route('users.index')
		                 ->with('success', 'Nhân viên đã tạo thành công!');
	}

	/**
	 * Show user data
	 *
	 * @param User $user
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
	 */
	public function show(User $user){
		return view('partials.auth.users.edit', [
			'user'     => $user,
			'userRole' => $user->roles->pluck('name')->toArray(),
			'roles'    => Role::latest()->get()
		]);
	}

	/**
	 * Edit user data
	 *
	 * @param User $user
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
	 */
	public function edit(User $user){
		return view('partials.auth.users.edit', [
			'user'     => $user,
			'userRole' => $user->roles->pluck('name')->toArray(),
			'roles'    => Role::latest()->get()
		]);
	}

	/**
	 * Update user data
	 *
	 * @param User $user
	 * @param UpdateUserRequest $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(User $user, UpdateUserRequest $request){
		$user->update($request->validated());
		$user->syncRoles($request->get('role'));

		return redirect()->route('users.index')
		                 ->withSuccess(__('Nhân viên được cập nhật thành công'));
	}

	/**
	 * Delete user data
	 *
	 * @param User $user
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(User $user){
		if ($user->delete()){
			return redirect()->route('users.index')
			                 ->withSuccess(__('Nhân viên đã xoá thành công.'));
		}
	}
}