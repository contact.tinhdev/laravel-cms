<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use OwenIt\Auditing\Models\Audit;

/**
 * Class AuditTrailController
 */
class AuditTrailController extends Controller{

	/**
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
	 */
	public function index(){
		$data = Audit::all();
dd($data);
		return view('audit.index', ['data' => $data]);
	}
}
