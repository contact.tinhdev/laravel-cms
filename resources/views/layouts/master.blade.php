<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>@yield('title',' | Metronic')</title>
    <meta name="description" content="Updates and statistics">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
		WebFont.load({
			google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
			active: function () {
				sessionStorage.fonts = true;
			}
		});
    </script>
    <link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/base/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/skins/header/base/light.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/skins/header/menu/light.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/skins/brand/light.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/skins/aside/light.css') }}" rel="stylesheet" type="text/css"/>

    @stack('style')

    <link rel="shortcut icon" href="{{ asset('assets/media/logos/favicon.ico') }}"/>
    <style>
		body {
			font-family: 'Roboto';
		}
    </style>
</head>
<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

@include('partials.components.header-mobile')

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">


        @include('partials.aside')

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            @include('partials.header')
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

            @include('partials.components.subheader')

            <!-- begin:: Content -->
                <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

                    <!--Begin::Dashboard 1-->
                @include('partials.notification')
                @yield('content')

                <!--End::Dashboard 1-->
                </div>

                <!-- end:: Content -->
            </div>

            @include('partials.footer')
        </div>
    </div>
</div>

@include ('partials.components.quick-panel')

@include ('partials.components.scrolltop')

@include ('partials.components.toolbar')

<script>
	var KTAppOptions = {
		"colors": {
			"state": {
				"brand": "#5d78ff",
				"dark": "#282a3c",
				"light": "#fff",
				"primary": "#5867dd",
				"success": "#34bfa3",
				"info": "#36a3f7",
				"warning": "#ffb822",
				"danger": "#fd3995"
			},
			"base": {
				"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
				"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
			}
		}
	};
</script>

<script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/base/scripts.bundle.js') }}" type="text/javascript"></script>
@stack('scripts')
<script src="{{ asset('assets/app/bundle/app.bundle.js') }}" type="text/javascript"></script>
</body>
</html>