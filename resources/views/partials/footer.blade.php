<!-- begin:: Footer -->
<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop">
    <div class="kt-footer__copyright">
        2021&nbsp;&copy;&nbsp;<a href="#" target="_blank" class="kt-link">TinhDev</a>
    </div>
    <div class="kt-footer__menu">
        <a href="#" target="_self" class="kt-footer__menu-link kt-link">Giới thiệu</a>
        <a href="#" target="_self" class="kt-footer__menu-link kt-link">Team</a>
        <a href="#" target="_self" class="kt-footer__menu-link kt-link">Liên hệ</a>
    </div>
</div>

<!-- end:: Footer -->