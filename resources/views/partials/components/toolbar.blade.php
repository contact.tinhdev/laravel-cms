<!-- begin::Sticky Toolbar -->
<ul class="kt-sticky-toolbar" style="margin-top: 30px;">
    <li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--success" id="kt_demo_panel_toggle" data-toggle="kt-tooltip" title="Check out more demos" data-placement="right">
        <a href="#" class=""><i class="flaticon2-drop" aria-hidden="true"></i></a>
    </li>
    <li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--brand" data-toggle="kt-tooltip" title="Builder" data-placement="left">
        <a href="#" target="_blank"><i class="flaticon2-gear" aria-hidden="true"></i></a>
    </li>
    <li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--warning" data-toggle="kt-tooltip" title="Tài liệu" data-placement="left">
        <a href="{{ route('ckfinder_examples') }}" target="_blank"><i class="flaticon2-telegram-logo" aria-hidden="true"></i></a>
    </li>
</ul><!-- end::Sticky Toolbar -->