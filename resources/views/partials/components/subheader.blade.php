<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title"> @yield('title')</h3>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        <span class="kt-subheader__desc">#XRS-45670</span>
        @yield('add_new')
    </div>
    <div class="kt-subheader__toolbar">
        <div class="kt-subheader__wrapper">

            <a href="#" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker">
                <span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title">Today</span>&nbsp;
                <span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date">{{date('d-m-Y', time())}}</span>
                <i class="flaticon2-calendar-1" aria-hidden="true"></i>
            </a>
            <div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="Quick actions" data-placement="left">
                <a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @include('partials.icons.sub-header-file-plus')
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <ul class="kt-nav">
                        <li class="kt-nav__section kt-nav__section--first">
                            <span class="kt-nav__section-text">Thêm mới:</span>
                        </li>
                        <li class="kt-nav__item">
                            <a href="{{ route('users.create') }}" class="kt-nav__link">
                                <i class="kt-nav__link-icon flaticon-user-add" aria-hidden="true"></i>
                                <span class="kt-nav__link-text">Nhân viên</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end:: Content Head -->