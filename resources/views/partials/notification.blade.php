@if(Session::get('success', FALSE))
	<?php $data = Session::get('success'); ?>
    @if (is_array($data))
        @foreach ($data as $msg)
            <div class="alert alert-success align-items-center" role="alert">
                <i class="fa fa-check pr-2" aria-hidden="true"></i>
                {{ $msg }}
            </div>
        @endforeach
    @else
        <div class="alert align-items-center alert-success" role="alert">
            <i class="fa fa-check pr-2" aria-hidden="true"></i>
            {{ $data }}
        </div>
    @endif
@endif
@if(Session::get('error', FALSE))
	<?php $data = Session::get('error'); ?>
    <div class="alert align-items-center alert-danger" role="alert">
        <i class="fa fa-window-close pr-2" aria-hidden="true"></i>
        {{ $data }}
    </div>
@endif
