@section('title', 'Dashboard')

@extends('layouts.master')

@section('content')
    <div class="kt-portlet" id="kt_portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon"><i class="flaticon-map-location" aria-hidden="true"></i></span>
                <h3 class="kt-portlet__head-title">Thư viện </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            @include('ckfinder::setup')
            <div id="ckfinder-widget"></div>
            <script>
	            CKFinder.widget( 'ckfinder-widget', {
		            width: '100%',
		            height: 700
	            } );
            </script>
        </div>
    </div>
@endsection
