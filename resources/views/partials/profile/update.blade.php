@section('title', 'Cập nhật hồ sơ '. Auth::user()->name)

@push('scripts')
    <script src="{{ asset('assets/app/general/crud/forms/validation/profile-form-controls.js') }}" type="text/javascript"></script>
@endpush
@section('nav_users', ' kt-menu__item--open kt-menu__item--here')@extends('layouts.master')
@section('content')
    <div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                @yield('title')
                            </h3>
                        </div>
                    </div>

                    <!--begin::Form-->
                    <form class="kt-form kt-form--label-right" action="{{ route('profile.update', Auth::user()->id) }}" method="POST" id="user-form">
                        @method('patch')
                        @csrf
                        <div class="kt-portlet__body">
                            <div class="mb-3">
                                <label for="name" class="form-label">Tên nhân viên</label>
                                <input value="{{ old('name')?old('name'):Auth::user()->name }}" type="text" class="form-control" name="name" placeholder="Name" required>

                                @if ($errors->has('name'))
                                    <span class="text-danger text-left">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input readonly value="{{ old('email')?old('email'):Auth::user()->email }}" type="email" class="form-control" name="email" placeholder="Email address" required>
                                @if ($errors->has('email'))
                                    <span class="text-danger text-left">{{ $errors->first('email') }}</span>
                                @endif
                            </div>

                            <div class="mb-3">
                                <label for="email" class="form-label">Mật khẩu</label>
                                <input id="password" value="{{ old('password')?old('password'):'' }}" type="password" class="form-control" name="password" autocomplete="off" placeholder="Nhập mật khẩu nếu có cần thay đổi">
                                @if ($errors->has('password'))
                                    <span class="text-danger text-left">{{ $errors->first('password') }}</span>
                                @endif
                            </div>

                            <div class="mb-3">
                                <label for="email" class="form-label">Cập nhật mật khẩu</label>
                                <input value="{{ old('password')?old('password'):'' }}" type="password" class="form-control" name="password_confirm" placeholder="Nhập lại mật khẩu nếu có cần thay đổi">
                                @if ($errors->has('password_confirm'))
                                    <span class="text-danger text-left">{{ $errors->first('password_confirm') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-9 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Cập nhật thông tin</button>
                                        <button onclick="history.back()" class="btn btn-secondary">Back</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <!--end::Form-->
                </div>

            </div>
        </div>
    </div>
@endsection
