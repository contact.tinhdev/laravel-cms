<!-- begin:: Aside -->
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close" aria-hidden="true"></i></button>
<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

    <!-- begin:: Aside -->
    <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
        <div class="kt-aside__brand-logo">
            <a href="#">
                <img alt="Logo" src="{{ asset('assets/media/logos/logo-dark.png') }}"/>
            </a>
        </div>
        <div class="kt-aside__brand-tools">
            <button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
                <span>@include('partials.icons.aside-angle-double-left')</span>
                <span>@include('partials.icons.aside-angle-double-right')</span>
            </button>
        </div>
    </div>

    <!-- end:: Aside -->

    <!-- begin:: Aside Menu -->
    <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
            <ul class="kt-menu__nav ">
                <li class="kt-menu__item  kt-menu__item--active" aria-haspopup="true">
                    <a href="index.html" class="kt-menu__link "><span class="kt-menu__link-icon">@include('partials.icons.menu-dashboard')</span><span class="kt-menu__link-text">Dashboard</span></a>
                </li>
                <li class="kt-menu__section ">
                    <h4 class="kt-menu__section-text">Hệ thống</h4>
                    <i class="kt-menu__section-icon flaticon-more-v2" aria-hidden="true"></i>
                </li>

                {{--  Begin: Audit trail --}}
                <li class="kt-menu__item " aria-haspopup="true">
                    <a target="_self" href="{{ route('audit.index') }}" class="kt-menu__link "><span class="kt-menu__link-icon">@include('partials.icons.menu-combined-shape')</span><span class="kt-menu__link-text">Audit trail</span></a>
                </li>
                {{--  End: Audit trail --}}
                {{--  Begin: Audit trail --}}
                <li class="kt-menu__item " aria-haspopup="true">
                    <a target="_self" href="#" class="kt-menu__link "><span class="kt-menu__link-icon">@include('partials.icons.aside-setting')</span><span class="kt-menu__link-text">Cài đặt</span></a>
                </li>
                {{--  End: Audit trail --}}

                <li class="kt-menu__section ">
                    <h4 class="kt-menu__section-text">Users</h4>
                    <i class="kt-menu__section-icon flaticon-more-v2" aria-hidden="true"></i>
                </li>
                <li class="kt-menu__item  kt-menu__item--submenu @yield('nav_users')" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <span class="kt-menu__link-icon">
                           @include('partials.icons.aside-shield-user')
                        </span><span class="kt-menu__link-text">Nhân viên</span>
                        <i class="kt-menu__ver-arrow la la-angle-right" aria-hidden="true"></i>
                    </a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item kt-menu__item--parent" aria-haspopup="true">
                                <span class="kt-menu__link"><span class="kt-menu__link-text">Nhân viên</span></span>
                            </li>
                            <li class="kt-menu__item  @yield('nav_users_add')" aria-haspopup="true">
                                <a href="{{ route('users.create') }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot" aria-hidden="true"><span></span></i>
                                    <span class="kt-menu__link-text">Thêm nhân viên</span>
                                </a>
                            </li>
                            <li class="kt-menu__item  @yield('nav_users_list')" aria-haspopup="true">
                                <a href="{{ route('users.index') }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot" aria-hidden="true"><span></span></i>
                                    <span class="kt-menu__link-text">Danh sách nhân viên</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
                {{--  Begin: Role --}}
                <li class="kt-menu__item @yield('nav_role')" aria-haspopup="true">
                    <a target="_self" href="{{ route('roles.index') }}" class="kt-menu__link "><span class="kt-menu__link-icon">@include('partials.icons.aside-user-group')</span><span class="kt-menu__link-text">Roles</span></a>
                </li>
                {{--  End: Role --}}

                {{--  Begin: Permission --}}

                <li class="kt-menu__item " aria-haspopup="true">
                    <a target="_self" href="{{ route('permissions.index') }}" class="kt-menu__link "><span class="kt-menu__link-icon">@include('partials.icons.aside-shield-user')</span><span class="kt-menu__link-text">Permission</span></a>
                </li>
                {{--  End: Permission --}}
            </ul>
        </div>
    </div>

    <!-- end:: Aside Menu -->
</div><!-- end:: Aside -->