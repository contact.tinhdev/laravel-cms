@push('style')
    <link href="{{ asset('assets/vendors/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>@endpush
@push('scripts')
    <script src="{{ asset('assets/vendors/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
		"use strict";
		var KTDatatablesBasicBasic = function () {
			var initTableUser = function () {
				var table = $('#table_user');
				table.DataTable({
					responsive: true,
					dom: `<'row'<'col-sm-12'tr>> <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
					lengthMenu: [5, 10, 25, 50],
					pageLength: 10,
					language: {'lengthMenu': 'Hiển thị _MENU_', 'dataTables_info': 'Hiển thị _INFO_',},
					order: [[1, 'desc']],
					columnDefs: [{targets: -1, orderable: false,}, {
						targets: 5,
						render: function (data, type, full, meta) {
							var status = {
								1: {'title': 'Không kích hoạt', 'state': 'danger'},
								2: {'title': 'Kích hoạt', 'state': 'success'},
							};
							if (typeof status[data] === 'undefined') {
								return data;
							}
							return '<span class="kt-badge kt-badge--' + status[data].state + ' kt-badge--dot"></span>&nbsp;' + '<span class="kt-font-bold kt-font-' + status[data].state + '">' + status[data].title + '</span>';
						},
					},],
				});
			};
			return {
				init: function () {
					initTableUser();
				},
			};
		}();
		jQuery(document).ready(function () {
			KTDatatablesBasicBasic.init();
		});

		function ConfirmDelete() {
			if (confirm('Bạn có chắc chắn muốn xoá? Thao tác không thể hoàn lại')) {
				return TRUE;
			}
			else {
				event.preventDefault();
			}
		}
    </script>@endpush
@section('add_new')
    <a href="{{ route('users.create') }}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
        Thêm mới
    </a>
@endsection
@section('title', 'Danh sách nhân viên')
@extends('layouts.master')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!--begin::Portlet-->

            <div class="kt-portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="kt-font-brand flaticon2-line-chart" aria-hidden="true"></i>
                            </span>
                        <h3 class="kt-portlet__head-title">
                            Danh sách nhân viên </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <a href="{{ route('users.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                                    <i class="la la-plus" aria-hidden="true"></i>
                                    Thêm mới
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body kt-portlet__body--fit p-3">
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="table_user">
                        <thead>
                        <tr>
                            <th>Record ID</th>
                            <th>Tên nhân viên</th>
                            <th>Email</th>
                            <th>Cập nhật lần cuối</th>
                            <th>Role</th>
                            <th>Trạng thái</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $key=> $user)
                            <tr>
                                <td>#{{$user->id }}</td>
                                <td>{{$user->name }}</td>
                                <td>{{$user->email }}</td>
                                <td>{{$user->updated_at }}</td>
                                <td>
                                    @foreach($user->roles as $role)
                                        <span class="badge bg-primary text-white">{{ $role->name }}</span>
                                    @endforeach
                                </td>
                                <td>{{$user->status }}</td>
                                <td>
                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"><i class="la la-edit" aria-hidden="true"></i></a>

                                    {{ Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline','onsubmit' => 'ConfirmDelete()']) }}
                                    {{ Form::button('<i class="la la-trash" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'btn btn-warning btn-sm btn-clean btn-icon btn-icon-md'] )  }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('nav_users', ' kt-menu__item--open kt-menu__item--here')
@section('nav_users_list', ' kt-menu__item--active')