@section('title', !empty($model)?$model->name:'Thêm nhân viên')
@push('scripts')
    <script src="{{ asset('assets/app/general/crud/forms/validation/users-form-controls.js') }}" type="text/javascript"></script>
@endpush
@extends('layouts.master')
@section('content')
    <div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                @yield('title')
                            </h3>
                        </div>
                    </div>

                    <!--begin::Form-->
                    <form class="kt-form kt-form--label-right" action="{{ route('users.store') }}" method="POST" id="user-form">
                        @csrf
                        <div class="kt-portlet__body">
                            <div class="mb-3">
                                <label for="name" class="form-label">Tên nhân viên</label>
                                <input value="{{ old('name') }}" type="text" class="form-control" name="name" placeholder="Name" required>

                                @if ($errors->has('name'))
                                    <span class="text-danger text-left">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input value="{{ old('email') }}" type="email" class="form-control" name="email" placeholder="Email address" required>
                                @if ($errors->has('email'))
                                    <span class="text-danger text-left">{{ $errors->first('email') }}</span>
                                @endif
                            </div>

                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-9 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Save user</button>
                                        <a href="{{ route('users.index') }}" class="btn btn-secondary">Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('nav_users', ' kt-menu__item--open kt-menu__item--here')@section('nav_users_add', ' kt-menu__item--active')