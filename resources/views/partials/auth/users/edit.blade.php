@section('title', !empty($user)?$user->name:'Thêm nhân viên')
@push('scripts')
    <script src="{{ asset('assets/app/general/crud/forms/validation/users-form-controls.js') }}" type="text/javascript"></script>
@endpush
@section('nav_users', ' kt-menu__item--open kt-menu__item--here')
@extends('layouts.master')
@section('content')
    <div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                @yield('title')
                            </h3>
                        </div>
                    </div>

                    <!--begin::Form-->
                    <form class="kt-form kt-form--label-right" action="{{ route('users.update', $user->id) }}" method="POST" id="user-form">
                        @method('patch')
                        @csrf
                        <div class="kt-portlet__body">
                            <div class="mb-3">
                                <label for="name" class="form-label">Tên nhân viên</label>
                                <input value="{{ old('name')?old('name'):$user->name }}" type="text" class="form-control" name="name" placeholder="Name" required>

                                @if ($errors->has('name'))
                                    <span class="text-danger text-left">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input value="{{ old('email')?old('email'):$user->email }}" type="email" class="form-control" name="email" placeholder="Email address" required>
                                @if ($errors->has('email'))
                                    <span class="text-danger text-left">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="role" class="form-label">Role</label>
                                <select class="form-control" name="role" required>
                                    <option value="">Select role</option>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}"
                                                {{ in_array($role->name, $userRole)
                                                    ? 'selected'
                                                    : '' }}>{{ $role->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('role'))
                                    <span class="text-danger text-left">{{ $errors->first('role') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-9 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Lưu lại</button>
                                        <a href="{{ route('users.index') }}" class="btn btn-secondary">Quay lại</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <!--end::Form-->
                </div>

            </div>
        </div>
    </div>
@endsection
