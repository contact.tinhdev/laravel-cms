@extends('layouts.master')
@section('title', 'Permissions')
@section('add_new')
    <a href="{{ route('permissions.create') }}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
        Thêm mới
    </a>
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!--begin::Portlet-->

            <div class="kt-portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="kt-font-brand flaticon2-line-chart" aria-hidden="true"></i>
                            </span>
                        <h3 class="kt-portlet__head-title">
                            Danh sách quyền </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <a href="{{ route('permissions.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                                    <i class="la la-plus" aria-hidden="true"></i>
                                    Thêm mới
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body kt-portlet__body--fit p-3">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col" width="15%">Tên Permissions</th>
                            <th scope="col">Guard</th>
                            <th scope="col" colspan="3" width="1%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($permissions as $permission)
                            <tr>
                                <td>{{ $permission->name }}</td>
                                <td>{{ $permission->guard_name }}</td>
                                <td>
                                    <a href="{{ route('permissions.edit', $permission->id) }}" class="btn btn-info btn-sm">Edit</a>
                                </td>
                                <td>
                                    {!! Form::open(['method' => 'DELETE','route' => ['permissions.destroy', $permission->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



@endsection