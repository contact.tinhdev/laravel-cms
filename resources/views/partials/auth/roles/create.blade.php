@extends('layouts.master')
@section('title', 'Tạo Role mới')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!--begin::Portlet-->

            <div class="kt-portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="kt-font-brand flaticon2-line-chart" aria-hidden="true"></i>
                            </span>
                        <h3 class="kt-portlet__head-title">
                            Thêm role mới </h3>
                    </div>

                </div>
                <div class="kt-portlet__body kt-portlet__body--fit p-3">
                    <form method="POST" action="{{ route('roles.store') }}">
                        @csrf
                        <div class="mb-3 form-group">
                            <label for="name" class="form-label">Tên role: </label>
                            <input value="{{ old('name') }}"
                                   type="text"
                                   class="form-control"
                                   name="name"
                                   placeholder="Nhập tên role" required>
                        </div>

                        <label for="permissions" class="form-label">Phân quyền</label>

                        <table class="table table-striped">
                            <thead>
                            <th scope="col" width="1%"><input type="checkbox" name="all_permission"></th>
                            <th scope="col" width="20%">Tên</th>
                            <th scope="col" width="1%">Guard</th>
                            </thead>

                            @foreach($permissions as $permission)
                                <tr>
                                    <td>
                                        <input type="checkbox"
                                               name="permission[{{ $permission->name }}]"
                                               value="{{ $permission->name }}"
                                               class='permission'>
                                    </td>
                                    <td>{{ $permission->name }}</td>
                                    <td>{{ $permission->guard_name }}</td>
                                </tr>
                            @endforeach
                        </table>

                        <button type="submit" class="btn btn-primary">Lưu lại</button>
                        <a href="{{ route('users.index') }}" class="btn btn-default">Quay lại</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
		$(document).ready(function() {
			$('[name="all_permission"]').on('click', function() {

				if($(this).is(':checked')) {
					$.each($('.permission'), function() {
						$(this).prop('checked',true);
					});
				} else {
					$.each($('.permission'), function() {
						$(this).prop('checked',false);
					});
				}

			});
		});
    </script>
@endsection
@section('nav_role', ' kt-menu__item--open kt-menu__item--here')