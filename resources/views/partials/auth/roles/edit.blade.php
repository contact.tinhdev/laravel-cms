@extends('layouts.master')
@section('add_new')
    <a href="{{ route('roles.create') }}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
        Thêm mới
    </a>
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!--begin::Portlet-->

            <div class="kt-portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="kt-font-brand flaticon2-line-chart" aria-hidden="true"></i>
                            </span>
                        <h3 class="kt-portlet__head-title">Chỉnh sửa role </h3>
                    </div>

                </div>
                <div class="kt-portlet__body kt-portlet__body--fit p-3">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form method="POST" action="{{ route('roles.update', $role->id) }}">
                        @method('patch')
                        @csrf
                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input value="{{ $role->name }}" type="text" class="form-control" name="name" placeholder="Name" required>
                        </div>

                        <label for="permissions" class="form-label">Phân quyền cho role</label>

                        <table class="table table-striped">
                            <thead>
                            <th scope="col" width="1%"><input type="checkbox" name="all_permission"></th>
                            <th scope="col" width="20%">Name</th>
                            <th scope="col" width="1%">Guard</th>
                            </thead>

                            @foreach($permissions as $permission)
                                <tr>
                                    <td>
                                        <input type="checkbox" name="permission[{{ $permission->name }}]" value="{{ $permission->name }}" class='permission'
                                                {{ in_array($permission->name, $rolePermissions)
                                                    ? 'checked'
                                                    : '' }}>
                                    </td>
                                    <td>{{ $permission->name }}</td>
                                    <td>{{ $permission->guard_name }}</td>
                                </tr>
                            @endforeach
                        </table>

                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <a href="{{ route('roles.index') }}" class="btn btn-default">Back</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script type="text/javascript">
		$(document).ready(function () {
			$('[name="all_permission"]').on('click', function () {

				if ($(this).is(':checked')) {
					$.each($('.permission'), function () {
						$(this).prop('checked', true);
					});
				}
				else {
					$.each($('.permission'), function () {
						$(this).prop('checked', false);
					});
				}

			});
		});
    </script>
@endpush
@section('nav_role', ' kt-menu__item--open kt-menu__item--here')