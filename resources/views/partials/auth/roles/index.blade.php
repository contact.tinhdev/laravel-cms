@extends('layouts.master')@section('title', 'Roles')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!--begin::Portlet-->

            <div class="kt-portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="kt-font-brand flaticon2-line-chart" aria-hidden="true"></i>
                            </span>
                        <h3 class="kt-portlet__head-title">
                            Danh sách Roles </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <a href="{{ route('roles.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                                    <i class="la la-plus" aria-hidden="true"></i>
                                    Thêm mới
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body kt-portlet__body--fit p-3">
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="table_user">
                        <thead>
                        <tr>
                            <th width="1%">S/N</th>
                            <th>Tên</th>
                            <th width="3%" colspan="3"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($roles as $key => $role)
                            <tr>
                                <td>#{{$role->id }}</td>
                                <td>{{$role->name }}</td>
                                <td>
                                    <a class="btn btn-info btn-sm" href="{{ route('roles.show', $role->id) }}">Xem</a>
                                </td>
                                <td>
                                    <a class="btn btn-primary btn-sm" href="{{ route('roles.edit', $role->id) }}">Sửa</a>
                                </td>
                                <td>
                                    {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Xoá',  ['class' => 'btn btn-danger btn-sm']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $roles->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('add_new')
    <a href="{{ route('roles.create') }}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
        Thêm mới
    </a>
@endsection
@section('nav_role', ' kt-menu__item--open kt-menu__item--here')