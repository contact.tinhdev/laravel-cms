@section('title', 'Dashboard')

@extends('layouts.master')

@section('content')
    <div class="kt-portlet" id="kt_portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon"><i class="flaticon-map-location" aria-hidden="true"></i></span>
                <h3 class="kt-portlet__head-title">This is admin </h3>
            </div>
        </div>
        <div class="kt-portlet__body"></div>
    </div>
@endsection
