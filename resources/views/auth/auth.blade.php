<div class="kt-login__signin">
    <div class="kt-login__head">
        <h3 class="kt-login__title">Đăng nhập vào Admin</h3>
    </div>
    <div class="kt-login__form">
        <form class="kt-form" action="" autocomplete="off">
            <div class="form-group">
                <input class="form-control" type="text" placeholder="Email" id="login-email" name="login-email" autocomplete="off">
            </div>
            <div class="form-group">
                <input class="form-control form-control-last" type="password" id="login-password" placeholder="Password" name="password">
            </div>
            <div class="kt-login__extra">
                <label class="kt-checkbox">
                    <input type="checkbox" id="remember" name="remember"> Ghi nhớ tôi
                    <span></span>
                </label>
                <a href="{{ route('password.request') }}" id="">Quên mật khẩu?</a>
            </div>
            <div class="kt-login__actions">
                <button id="kt_login_signin_submit" class="btn btn-brand btn-pill btn-elevate">Đăng nhập</button>
            </div>
        </form>
    </div>
</div>
<div class="kt-login__signup">
    <div class="kt-login__head">
        <h3 class="kt-login__title">Đăng ký</h3>
        <div class="kt-login__desc">Điền đầy đủ thông tin để tạo tài khoản mới!</div>
    </div>
    <div class="kt-login__form">
        <form class="kt-form" action="">
            <div class="form-group">
                <input class="form-control" type="text" placeholder="Họ & tên" name="fullname">
            </div>
            <div class="form-group">
                <input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off">
            </div>
            <div class="form-group">
                <input class="form-control" type="password" placeholder="Mật khẩu" name="password">
            </div>
            <div class="form-group">
                <input class="form-control form-control-last" type="password" placeholder="Nhập lại mật khẩu" name="rpassword">
            </div>
            <div class="kt-login__actions">
                <button id="kt_login_signup_submit" class="btn btn-brand btn-pill btn-elevate">Đăng ký</button>
                <button id="kt_login_signup_cancel" class="btn btn-outline-brand btn-pill">Huỷ bỏ</button>
            </div>
        </form>
    </div>
</div>
<div class="kt-login__account">
    <span class="kt-login__account-msg">Không có tài khoản?</span>&nbsp;&nbsp;<a href="javascript:;" id="kt_login_signup" class="kt-login__account-link">Đăng ký!</a>
</div>
@push('child-script')
    <script>
		"use strict";

		// Class Definition
		var KTLoginGeneral = function () {

			var login = $('#kt_login');

			var showErrorMsg = function (form, type, msg) {
				var alert = $('<div class="kt-alert kt-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div>');

				form.find('.alert').remove();
				alert.prependTo(form);
				KTUtil.animateClass(alert[0], 'fadeIn animated');
				alert.find('span').html(msg);
			}

			var displaySignUpForm = function () {
				login.removeClass('kt-login--forgot');
				login.removeClass('kt-login--signin');

				login.addClass('kt-login--signup');
				KTUtil.animateClass(login.find('.kt-login__signup')[0], 'flipInX animated');
			}

			var displaySignInForm = function () {
				login.removeClass('kt-login--forgot');
				login.removeClass('kt-login--signup');

				login.addClass('kt-login--signin');
				KTUtil.animateClass(login.find('.kt-login__signin')[0], 'flipInX animated');
			}

			var displayForgotForm = function () {
				login.removeClass('kt-login--signin');
				login.removeClass('kt-login--signup');

				login.addClass('kt-login--forgot');
				KTUtil.animateClass(login.find('.kt-login__forgot')[0], 'flipInX animated');

			}

			var handleFormSwitch = function () {
				$('#kt_login_forgot').click(function (e) {
					e.preventDefault();
					displayForgotForm();
				});

				$('#kt_login_forgot_cancel').click(function (e) {
					e.preventDefault();
					displaySignInForm();
				});

				$('#kt_login_signup').click(function (e) {
					e.preventDefault();
					displaySignUpForm();
				});

				$('#kt_login_signup_cancel').click(function (e) {
					e.preventDefault();
					displaySignInForm();
				});
			}

			var handleSignInFormSubmit = function () {
				$('#kt_login_signin_submit').click(function (e) {
					e.preventDefault();
					var btn = $(this);
					var form = $(this).closest('form');
					var email = $('#login-email').val();
					var password = $('#login-password').val();
					var remember = $('#remember').val();

					form.validate({
						rules: {
							email: {
								required: true,
								email: true
							},
							password: {
								required: true
							}
						},
						messages: {
							email: {
								required: "Vui lòng nhập email",
								email: "Email chưa đúng định dạng"
							},
							password: {
								required: "Vui lòng nhập mật khẩu"
							}
						}
					});

					if (!form.valid()) {
						return;
					}

					btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

					form.ajaxSubmit({
						url: '{{ route('login') }}',
						method: 'POST',
						data: {
							_token: '{{ csrf_token() }}',
							email: email,
							password: password,
							remember: remember,
						},
						success: function (response, status, xhr, $form) {
							// similate 2s delay
							window.location.reload();
						},
						error: function (jqXHR, textStatus, errorThrown) {
							var errorMessage = jqXHR.responseText;
							setTimeout(function () {
								btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
								showErrorMsg(form, 'danger', 'Tài khoản hoặc mật khẩu chưa đúng, vui lòng kiểm tra lại!');
							}, 2000);
						}
					});
				});
			}

			var handleSignUpFormSubmit = function () {
				$('#kt_login_signup_submit').click(function (e) {
					e.preventDefault();

					var btn = $(this);
					var form = $(this).closest('form');

					form.validate({
						rules: {
							fullname: {
								required: true
							},
							email: {
								required: true,
								email: true
							},
							password: {
								required: true
							},
							rpassword: {
								required: true
							},
						},
						messages: {
							fullname: {
								required: "Họ và tên không được để trống"
							},
							email: {
								required: "Vui lòng nhập email",
								email: "Email chưa đúng định dạng"
							},
							password: {
								required: "Vui lòng nhập mật khẩu"
							},
							rpassword: {
								required: "Vui lòng nhập mật khẩu"
							},
						},
					});

					if (!form.valid()) {
						return;
					}

					btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

					form.ajaxSubmit({
						url: '',
						success: function (response, status, xhr, $form) {
							// similate 2s delay
							setTimeout(function () {
								btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
								form.clearForm();
								form.validate().resetForm();

								// display signup form
								displaySignInForm();
								var signInForm = login.find('.kt-login__signin form');
								signInForm.clearForm();
								signInForm.validate().resetForm();

								showErrorMsg(signInForm, 'success', 'Thank you. To complete your registration please check your email.');
							}, 2000);
						}
					});
				});
			}


			// Public Functions
			return {
				// public functions
				init: function () {
					handleFormSwitch();
					handleSignInFormSubmit();
					handleSignUpFormSubmit();
				}
			};
		}();

		// Class Initialization
		jQuery(document).ready(function () {
			KTLoginGeneral.init();
		});
    </script>
@endpush