@extends('layouts.app')
@section('auth_active', 'kt-login--signup')

@section('content')
    @include('auth.auth')
@endsection
