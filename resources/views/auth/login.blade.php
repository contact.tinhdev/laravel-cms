@extends('layouts.app')
@section('auth_active', 'kt-login--signin')

@section('content')
    @include('auth.auth')
@endsection
