@extends('layouts.app')@section('auth_active', 'kt-login--forgot')

@section('content')

    <div class="kt-login__forgot">
        <div class="kt-login__head">
            <h3 class="kt-login__title">Quên mật khẩu?</h3>
            <div class="kt-login__desc">Nhập email để khôi phục lại mật khẩu:</div>
        </div>
        <div class="kt-login__form">
            <form method="POST" class="kt-form" action="{{ route('password.email') }}">
                @csrf

                <div class="form-group">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
                <div class="kt-login__actions">
                    <button type="submit" id="kt_login_forgot_submit" class="btn btn-brand btn-pill btn-elevate">Gửi yêu cầu</button>
                    <a href="{{ route('login') }}" style="line-height: 28px;" id="kt_login_forgot_cancel" class="btn btn-outline-brand btn-pill">Huỷ bỏ</a>
                </div>
            </form>
        </div>
    </div>
@endsection
