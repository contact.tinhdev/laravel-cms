#CÁCH CÀI ĐẶT
Lệnh: 
1. Cấu hình file env db
2. composer install
3. php artisan key:generate
4. php artisan migrate
5. php artisan db:seed --class=CreateAdminUserSeeder
6. php artisan permission:create-permission-routes
7. php artisan serve
8. Mở trình duyệt, đường dẫn 127.0.0.1:8000